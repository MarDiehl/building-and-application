============
Introduction
============
Compiling an Fortran or C application contained in a single file is a simple task.
Syntax errors are easy to fix, especially since compilers usually give helpful error messages.
However, even small real world codes are typically split up into multiple files and rely on external libraries.
Combining source or object code from different locations is more complicated process which is usually performed with the help of different tools.
While these tools are very helpful, they hide the underlying process from the user.
Still, understanding in detail how compiler, linker, and build system work together is helpful to resolve issues if a build process fail.

This tutorial briefly shows the underlying concepts used when building a library or an executable on linux.
