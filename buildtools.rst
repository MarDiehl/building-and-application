***********
Build Tools
***********
Using ``make`` speeds up compilation and builds a whole project with one command instead of manually compiling and linking.
However, it still requires to explicitly describe the dependencies in the ``Makefile``.
Several tools exist that automate the generation of make files or automate the build process without make

Autotools
=========
Autotools are part of the `GNU Build System`.
The autotools create a ``Makefile`` adjusted to the current system, taking e.g. the processor type into account.
The actual configuration step is followed by a

.. code-block:: shell

   ./configure
   make
   make install

CMake
=====
CMake creates build instructions for different build systems such as ``make`` or ``Ninja``.
Here we assume that make is used, which is the default.
In contrast to the Autotools, Cmake does an out of directory build, i.e. the object code is not placed in the same directory as the source code.
A typical build with Cmake starts therefore with the creation of a directory, typically called ``build``.
That means, if you are in the source code directory, run:

.. code-block:: shell

   mkdir build
   cd build
   cmake ..
   make
   make install

CMake is configured by one or multiple files called ``CMakeLists.txt``.

Additional Information
======================
- https://cmake.org
- https://www.siliceum.com/en/blog/post/cmake_01_cmake-basics
- https://ninja-build.org
- https://mesonbuild.com
