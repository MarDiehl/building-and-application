**********************
Command-line interface
**********************
A Command-line interface (CLI) enables interaction with the operation system via the keyboard.
All examples in this tutorial are CLI-based.


Variants
========
A large number of CLIs exists.
For unix-like operating system, the POSIX standard is the base for different CLIs usually called `shell`:

- Bourne shell (`sh`)
- Bash (`bash`)
- Z shell (`zsh`)

.. note::

   Use *zsh* with the *Oh My Zsh* extension!


Basic Commands
==============
These are the most basic commands.

- List the content (files and directorys) of the current directory:

  .. code-block:: shell
  
     ls [OPTION]... [FILE]...
  
- Change directory:
  
  .. code-block:: shell
  
     cd [DIRECTORY]
  
- Copy a file or directory:
  
  .. code-block:: shell
  
     cp [OPTION]... [-T] SOURCE DEST
  
- Move/Rename a file or directory:
  
  .. code-block:: shell
  
     mv [OPTION]... [-T] SOURCE DEST
  
- Output/print to the shell:
  
  .. code-block:: shell
  
     echo 'hello world'


Variables
=========
Like in any programming language, it is possible to store values in variables

.. code-block:: shell

   VAR=hello

The value of a variable can be accessed with the dollar sign (``$``)

.. code-block:: shell

   echo $VAR

Note that certain variables have a special meaning:

-  ``PATH``: Valid locations for executables, e.g. ``/usr/bin/``. Individual locations are separated by colons.
-  ``PWD``: The current working directocry, short for `print working directory`.


Redirecting
===========
The output of a command can be `redirected` to a file:

.. code-block:: shell

   echo 'hello world' > my_file

.. warning::
   This overwrites the content of the file `my_file`

Appending to an existing file works with a double angle bracket:

.. code-block:: shell

   echo 'first line' > my_file
   echo 'second line' >> my_file


Scripting
=========
More complicated series of commands can be stored in a file for later execution:

.. code-block:: shell

   echo 'ls' > show_directory
   sh ./show_directory

To enable execution of the file without explicitly calling the interpreter (here ``sh``), a so-called shebang needs to be present at the top of the file:

.. code-block:: shell

   echo '#!sh' > hello
   echo 'echo hello world' >> hello

This tells the shell to use `sh` to execute the script.
So, we can make the file executable and execute it:

.. code-block:: shell

   chmod +x hello
   ./hello


Note that the interpreter does not have to be a shell.
Other valid interpreters are ``phython3`` or ``julia``.
Usually, the interpeter is not directly specified but the command ``env`` in ``/usr/bin`` is asked to provide its location.
Hence, common shebangs are

.. code-block:: shell

   #!/usr/bin/env sh
   #!/usr/bin/env python3


Locate a Command
================

Each command is a file which is located somewhere on the hard disk/solid state drive.
The location can be shown with:

.. code-block:: shell

   which [COMMAND]


.. code-block:: shell

   which cp

shows, for example, that ``cp`` is located in ``/usr/bin/`` which is a typical location on linux for commands that are system-wide available.
Files located in other directories cannot be executed.
That is why ``./hello`` is invoked with a leading ``./``.
This tells the shell to execute the file from the current working directory even though it is (most likely) not in ``PATH``.
Alternatively, the full path can be given, e.g.

.. code-block:: shell

   $PWD/hello

To make hello accesible from everywhere, add the current working directory to the ``PATH`` variable:

.. code-block:: shell

   PATH=$PWD:$PATH

The script can now be executed from everywhere:

.. code-block:: shell

   cd /tmp
   hello


Startup Files
=============

Startup files are a convenient means to set common variables automatically.
Common startup files are are ``~/.bashrc`` (for `bash`) and ``~/.zshrc`` (for `zsh`).

.. note::

   A leading dot in the file name is used to hide files on linux.


.. note::

   The tilde ``~`` denotes the home directory, it is equivalent to the variable ``$HOME``.






Additional Information
======================
- https://ohmyz.sh
- https://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html
