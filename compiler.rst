========
Compiler
========

A compiler translates source code (human readable) into machine code.
This tutorial shows how to compile Fortran and C code on Linux and demonstrates how to inspect the resuling machine code.
The names of the GNU compilers that are used here are ``gfortran`` for Fortran and ``gcc`` for C.


Main Program
============
The simplest valid Fortran and C code is an empty file.
We create two empty files with ``touch``:

.. code-block:: shell

   touch fortran.f90
   touch c.c

``touch`` updates the time stamp of a file to the current time.
If the given file does not exists it creates a new, empty file with current time stamp.

To compile these files, call the compiler with the ``-c`` option, which instructs the compiler to perform only the compilation:

.. code-block:: shell

   gfortran -c fortran.f90
   gcc -c c.c

the results are two object files called ``c.o`` and ``fortran.o``.
The extension ``.o`` indicates that this is an object file which contains machine code.
In contrast to the source code files, which have a size of zero bytes, the object files have a small but finite size.

We can inspect the content of the files with `nm`, a tool that list symbols, e.g. functions, in object files:

.. code-block:: shell

   nm fortran.f90
   nm c.c

As expected, no symbols exist in these files.


As a slightly more complicated example, we use the following code

.. code-block:: Fortran

   program HelloWorld
     implicit none
     call Hello(1)
   end program HelloWorld


.. code-block:: C

   int main() {
     int example = 2;
     hello_(&example);
     return 0;
   }

when saved as `HelloWorld.f90` and `hello_world.c`, compilation is done with

.. code-block:: sh

   gfortran -c HelloWorld.f90
   gcc -c hello_world.c

.. note::
   Fortran calls are ``by reference``, the definition of ``int`` instead of using the literal ``2`` and the call with ``&`` in `hello_world.c` ensures that both programs are compatible with the same hello function.


Note that `Hello` (Fortran) and `hello_` (C) references to an undefined function.
The C compiler gives therefore a warning, which we ignore for the moment.
A similar warning is issued for the Fortran code if it is compiled with `-Wimplicit-procedure`.

Inspection of the files show that they indeed contain symbols

.. code-block:: sh

   nm HelloWorld.o
   nm hello_world.o

Both objects contain, besides some language dependent stuff, the same symbols:
An undefined (``U``) reference to ``hello_`` and an existing definition (``T``) of ``main``.
Note the name change of the Fortran function:
Since Fortran is case insensitive, letters are converted to lower case to resolve ambiguities.

Moreover, a trailing underscore is added.
This behavior is called `name mangling`.


Individual Function
===================
We now implement the missing ``hello`` function,

.. code-block:: Fortran

   subroutine Hello(example)
     implicit none
     integer, intent(in) :: example
     print*, 'hello from example ', example
   end subroutine Hello

save it as ``Hello.f90`` and compile with

.. code-block:: sh

   gfortran -c Hello.f90

inspection with ``nm`` reveals that the symbol ``hello_`` exists.
The other, undefined (`U`) functions refer to the Fortran runtime library which provides the `print` statement.


Additional Information
======================
- https://man7.org/linux/man-pages/man1/nm.1.html
- https://stackoverflow.com/questions/1314743
