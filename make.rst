****
Make
****
Manually compiling and linking the code of a large project is a tidious task.
It is usually automated using ``make``, which is a program that executes instructions given in a `Makefile`.
The instructions are given in the form of `dependencies` and `targets`.
Creation of a file, e.g. usually compilation of source code into object code, is a target.
This target might depend on other targets.
Consider the following case:

.. code-block:: make

  HelloWorld.exe : HelloWorld.o Hello.o
     gfortran -o HelloWorld.exe HelloWorld.o Hello.o

  HelloWorld.o : HelloWorld.f90
     gfortran -c HelloWorld.f90

  Hello.o : Hello.f90
     gfortran -c Hello.f90

The target ``HelloWorld.exe`` depends on the object code in ``HelloWorld.o`` and ``Hello.o`` which in turn depend on their repective source files.
The clou of ``make`` is the consideration of time stamps which avoids unessessary compilation:
If ``Hello.o`` is newer than ``Hello.f90``, no action is needed for this target!

.. note::
   It is good practise to use variables for the name of the compiler.
   Common names are ``FC`` or ``F90`` for Fortran and ``CC`` for C.
   Building is then iniated with ``make FC=gfortran CC=gcc``.

.. warning::

   The indentation in makefiles needs to be done with tab stops.

The GNU version of ``make`` searches for files with the names ``GNUmakefile``, ``makefile``, and ``Makefile`` (in that order).
It then starts to build the first target.
It is also possible to specify the target explicitly.
For example, to just compile ``Hello.f90`` into object code, run

.. code-block:: sh

  make Hello.o

Common targets are ``all``, ``install``, and ``clean``.

Additional Information
======================
https://www.gnu.org/software/make/manual/make.html
