==========
Interfaces
==========

In the previous example, an unknown function was called in the main function.
This can lead to undefined behavior, for example when a function that expects an array of length ten is called with an array of length five.

.. note::
   The term ``actual argument`` is used in C and Fortran to denote the argument when calling a function.
   For arguments in the function definition, the terms are ``dummy argument`` in Fortran and ``formal argument`` in C.

The following example illustrates the problem:

.. code-block:: Fortran
   
   ! Add.f90
   subroutine Add(a,b)
     integer, intent(in) :: a, b
      print*, a+b
   end subroutine Add

.. code-block:: Fortran

   ! RunTimeError.f90
   program RunTimeError
     call add()
   end program RunTimeError


.. code-block:: sh
   
   gfortran Add.f90 RunTimeError.f90 -o crash
   ./crash

.. note::
   It cannot be guaranteed that this program crashes, but since there is no memory allocated for ``a`` and ``b``, normally the program terminates with a `segmentation fault`.


Blindly linking object code is dangerous and should be avoided.
Both, C and Fortran, therefore have means to provide information about existing functions.


Fortran
=======

Fortran provides different approaches to guarantee that a funcion or subroutine is called with the correct arguments.

Interfaces
----------

The explicit definition of an interface is the manual repetition of the function signature

.. code-block:: Fortran

   ! CompileTimeError.f90
   program CompileTimeError
     interface
       subroutine Add(a,b)
         integer, intent(in) :: a, b
       end subroutine Add
     end interface
     call Add()
   end program CompileTimeError

.. code-block:: sh
   
   gfortran Add.f90 CompileTimeError.f90


Now the compiler can detect the invalid call to ``Add``.
Using interfaces is only a small improvement, because it is still the programmers responsibility to write a correct interface.


Modules
-------

Modules are a feature to automatically create interface definitions

.. code-block:: Fortran
   
   ! Math.f90
   module Math
     contains
     subroutine Add(a,b)
       integer, intent(in) :: a,b
         print*, a+b
     end subroutine Add
   end module Math

.. code-block:: Fortran

   ! BestPractice.f90
   program BestPractice
     use Math
     call Add(1,11) ! Add() or Add("1","11") would result in a compile time error
   end program BestPractice

.. code-block:: sh
   
   gfortran Add.f90 BestPractice.f90 -o BestPractice
   ./BestPractice

The compiler searches for module files in directories specified with the ``-I`` option and in the current directory.

.. note::
   The information contained in modules is usually stored in files with the ``.mod`` extension.
   The seach path for module files is given via the ``-I`` (for include) option.

Submodules
----------

The drawback of modules is the strict hierarchy that is introduces.
If a module or a program uses a module, this module needs to be compiled earlier.

Submodules can be understood as a mixture of ``interface`` and ``module``.


C
=
.. code-block:: C

   /* math.h */
   void __math_MOD_add (int *a, int *b);

.. code-block:: C

   /* best_practice.h */
   #include "math.h"
   int main()
   {
      int x = 1;
      int y = 11;
      __math_MOD_add(&x,&y);
   }

.. code-block:: sh
   
   gfortran math.c best_practice.c -o best_practice
   ./best_practice

By default, gcc searches ``/usr/local/include/`` and ``/usr/include/`` for header files.
Additional locations can be specified with the ``-I`` option or via the ``C_INCLUDE_PATH`` environment variable.
